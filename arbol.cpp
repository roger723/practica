#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <list>

using namespace std;

template <typename N>
class Nodo
{
    public:
        N dato;
        Nodo* hijos[2];
        int altura;
        Nodo(N n)
        {
            dato=n;
            hijos[0]=nullptr;
            hijos[1]=nullptr;
            altura=0;
        };
        virtual ~Nodo(){};
};

template <typename t>
struct asc{bool operator ()(t &a, t &b){return a<b;}};

template <typename t>
struct des{bool operator ()(t &a, t &b){return a>b;}};

template<class T>
struct traitAsc{
    typedef T tipo;
    typedef asc<tipo> cmp;
};

template<class T>
struct traitDes{
    typedef T tipo;
    typedef des<tipo> cmp;
};

template <typename Tr>
class treeAvl
{
    public:
        typedef typename Tr::tipo T;
        typedef typename Tr::cmp cmp;
        typedef Nodo<T> nodo;

        treeAvl(){root=nullptr;};
        virtual ~treeAvl(){};

        bool insertar(T n)
        {
            nodo** p;
            if (buscar(n,p)) return false;
            nodo* q =new nodo(n);
            *p=q;
            recorrer_rama();
            return true;
        };

        bool remover (T n)
        {
            nodo** p;
            if (!buscar(n,p)) return 0;
            if ((*p)->hijos[0]==nullptr and (*p)->hijos[1]==nullptr) borrado1(p);
            else if ((*p)->hijos[0]==nullptr or (*p)->hijos[1]==nullptr) borrado2(p);
            else borrado3(p);
            return 1;
        };

        void guardar()
        {
            ofstream archivo;
            archivo.open("arbolito.dot");
            archivo.clear();
            archivo<<"//Arbol Avl \n";
            archivo<<'\n';
            archivo<<"digraph arbolito {\n";
            if (root){
                archivo<<conv_num_str(root->dato)+ "; \n";
                archivo<<armar_cadena(root);}
            archivo<<"}";
            archivo.close();
        };

};
